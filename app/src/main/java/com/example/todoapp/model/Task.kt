package com.example.todoapp.model

class Task (var id : Int, var urgent: Boolean, var nome : String, var done: Boolean) {

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Task
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }

    override fun toString(): String {
        return "Task(urgent=$urgent, nome='$nome')"
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


}