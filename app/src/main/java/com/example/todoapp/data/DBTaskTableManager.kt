package com.example.todoapp.data

import android.content.ContentValues
import android.content.Context
import com.example.todoapp.model.Task
import java.util.ArrayList

class DBTaskTableManager(context : Context) {


    private val dbHelper = DBHelper(context)

    companion object{

        private var COLUMNS = arrayOf<String>("t_id", "t_name", "t_urgent", "t_done")

    }

    fun insert(task : Task) : Long {
        val cv = ContentValues()
        cv.put(DBSchema.TaskTable.NAME, task.nome)
        cv.put(DBSchema.TaskTable.URGENT, task.urgent)
        cv.put(DBSchema.TaskTable.DONE, task.done)
        val db = dbHelper.writableDatabase
        val id = db.insert(DBSchema.TaskTable.TABLENAME, null, cv)
        db.close()
        return id
    }


    fun delete(task : Task){
        val db = dbHelper.writableDatabase
        val whereclause = "${DBSchema.TaskTable.ID} = ?"
        db.delete(DBSchema.TaskTable.TABLENAME, whereclause, arrayOf(task.id.toString()))
        db.close()
    }

    fun update(task : Task){
        val db = dbHelper.writableDatabase
        val cv = ContentValues()
        cv.put(DBSchema.TaskTable.DONE, task.done)
        val whereclause = "${DBSchema.TaskTable.ID} = ?"
        db.update(DBSchema.TaskTable.TABLENAME, cv, whereclause, arrayOf(task.id.toString()))
    }

    fun getAllTasks() : ArrayList<Task>{
        val tasks = ArrayList<Task>()
        val db = dbHelper.writableDatabase
        val cur = db.query(
            DBSchema.TaskTable.TABLENAME,
            COLUMNS,
            null,
            null,
            null,
            null,
            DBSchema.TaskTable.TIMESTAMP + " DESC"
        )

        while(cur.moveToNext()){
            val id     = cur.getLong(0)
            val name   = cur.getString(1)
            val urgent = cur.getInt(2) == 1
            val done   = cur.getInt(3) == 1
            val task = Task(id.toInt(), urgent, name, done)
            tasks.add(task)
        }

        return tasks
    }
}