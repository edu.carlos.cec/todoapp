package com.example.todoapp.data

import android.content.Context
import com.example.todoapp.model.Task

object TaskSingleton {

    private lateinit var tasksSaved : ArrayList<Task>
    private lateinit var dbTasks    : DBTaskTableManager

    private fun initDAO(context: Context) {
        if(!::tasksSaved.isInitialized){
            this.tasksSaved = ArrayList<Task>()
            this.dbTasks = DBTaskTableManager(context)
        }
    }

    //CREATE
    fun add(context : Context, task: Task) : Int{
        initDAO(context)
        val id = this.dbTasks.insert(task)
        task.id = id.toInt()
        tasksSaved.add(0, task)
        return 0
    }

    //READ
    fun getAllTasks(context: Context) : ArrayList<Task> {
        initDAO(context)
        this.tasksSaved = this.dbTasks.getAllTasks()
        return tasksSaved
    }

    //UPDATE
    fun update(context: Context, task : Task) : Int{
        initDAO(context)
        val pos = tasksSaved.indexOf(task)
        tasksSaved[pos].done = task.done
        this.dbTasks.update(task)
        return pos
    }

    //DELETE
    fun delete(context: Context, task: Task) : Int{
        initDAO(context)
        //Deletar no Banco de Dados
        val pos = this.tasksSaved.indexOf(task) //Temos que achar primeiro a posição da task dentro do ArrayList, para podermos passar para o RecyclerView a posição para o update das tasks apresetadas
        tasksSaved.removeAt(pos)
        this.dbTasks.delete(task)
        return pos
    }


}