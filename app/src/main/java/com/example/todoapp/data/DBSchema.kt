package com.example.todoapp.data

object DBSchema {

    //Schema da Tabela dentro do Banco de Dados
    object TaskTable{
        const val TABLENAME = "Task"
        const val ID        = "t_id"
        const val NAME      = "t_name"
        const val URGENT    = "t_urgent"
        const val DONE      = "t_done"
        const val TIMESTAMP = "t_timestamp"

        fun getCreateTableQuery() : String {
            return """
            CREATE TABLE IF NOT EXISTS $TABLENAME
             (
                $ID     INTEGER PRIMARY KEY AUTOINCREMENT,
                $NAME   TEXT NOT NULL,
                $URGENT BOOLEAN NOT NULL,
                $DONE   BOOLEAN NOT NULL, 
                $TIMESTAMP TEXT DEFAULT CURRENT_TIMESTAMP
             )
        """.trimIndent()
        }
    }




}