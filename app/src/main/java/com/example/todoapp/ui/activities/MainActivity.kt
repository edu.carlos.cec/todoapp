package com.example.todoapp.ui.activities

import android.content.Context
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.SparseBooleanArray
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.util.forEach
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.data.TaskSingleton
import com.example.todoapp.model.Task
import com.example.todoapp.ui.lists.task.TaskListAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {


    private lateinit var adapter : TaskListAdapter

    //Views do layout ===============================================
    private lateinit var btnOk                : Button       //Botão de ok
    private lateinit var eTextTask            : EditText     //Botão de Editar o Texto
    private lateinit var switchUrgent         : Switch       //Botão de Switch
    private lateinit var isDoneTask           : CheckBox     //Checkbox de not done tasks
    private lateinit var rvTaskList           : RecyclerView
    private lateinit var removeTaskButton     : FloatingActionButton
    //===============================================================



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //==========================================================================================
        //Podemos chamar esses elementos do layout para nosso código
        this.btnOk              = findViewById(R.id.btnOk)         //Pegando o botao de ok
        this.eTextTask          = findViewById(R.id.eTextTask)     //Pegando a view de input da task
        this.switchUrgent       = findViewById(R.id.switchUrgent)  //Pegando o view switch
        this.isDoneTask         = findViewById(R.id.isDoneTask)    //Pegando o checkbox
        this.rvTaskList         = findViewById(R.id.rvTaskList)
        this.removeTaskButton   = findViewById(R.id.removeTaskButton)
        //==========================================================================================


        //==========================================================================================
        //==========================================================================================
        //Código do RecyclerView ===================================================================
        this.rvTaskList.layoutManager = LinearLayoutManager(this)
        this.rvTaskList.setHasFixedSize(true)
        this.adapter = TaskListAdapter(TaskSingleton.getAllTasks(this))
            .setOnClickTaskListener{
                val pos = TaskSingleton.update(baseContext, it)
                rvTaskList.adapter?.notifyItemChanged(pos)
            }.setOnChangeSelectionListener{
                if(it.size() > 0){
                    this.removeTaskButton.visibility = View.VISIBLE
                } else {
                    this.removeTaskButton.visibility = View.GONE
                }
            }
        this.rvTaskList.adapter = this.adapter
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================


    }



    //==============================================================================================
    //                                      CRIAÇÃO
    //==============================================================================================
    fun onClickBtnOk(v: View){
        //Esconder o teclado
        hideKeyBoard()
        //Pegando o valor do texto no input da Tarefa
        val txtTask: String = this.eTextTask.text.toString()
        var pos = 0;
        if( txtTask.isNotEmpty() ) {
            pos = TaskSingleton.add(this, Task(0, this.switchUrgent.isChecked , txtTask, false))
            this.rvTaskList.adapter?.notifyItemInserted(pos) //Chama o bind nessa posição
        } else {
            Toast.makeText(this, R.string.empty_task, Toast.LENGTH_SHORT).show()
        }

        //Precisamos reiniciar o valor do input
        this.eTextTask.setText("")
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //==============================================================================================
    fun onClickIsDone(v: View){
        val checkbox = v as CheckBox
        //Criando uma nova lista com as tarefas não feitas
        (this.rvTaskList.adapter as TaskListAdapter).filterTaskDone(checkbox.isChecked)
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //                                      REMOVER
    //==============================================================================================
    fun onClickDelete(v : View){
        this.adapter.selectedTasks.forEach { _, value ->
            val pos = TaskSingleton.delete(this, value)
            this.adapter.notifyItemRemoved(pos)
        }
        this.adapter.selectedTasks.clear()
        this.removeTaskButton.visibility = View.GONE
     }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                      HIDE THE KEYBOARD
    //==============================================================================================
    private fun hideKeyBoard(){
        if(currentFocus != null){
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            //Escondendo o input
            imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
        }
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



}