package com.example.todoapp.ui.lists.task

import android.content.DialogInterface
import android.graphics.Color
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.data.TaskSingleton
import com.example.todoapp.model.Task

//O itemView é como toda a View
//O construtor da ViewHolder deve sempre definir seu itemView para a superclasse
//Recebemos o adapter para ter acesso ao listener
class TaskListViewHolder(

    itemView : View,
    private val adapter: TaskListAdapter

) : RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {

    //Aqui temos todas as views que precisa ser guardadas
    private val color    : FrameLayout = itemView.findViewById(R.id.colorTask)
    private val etxtTask : TextView = itemView.findViewById(R.id.etxtTask)
    private val doneTask : CheckBox = itemView.findViewById(R.id.doneTask)
    private lateinit var task : Task //Mantem a informacao da Task

    //Vincula a task com as Views
    //Aqui acontece a reciclagem da ViewHolder
    //Será chamado toda vez que houver necessidade de reciclar os dados da itemView
    fun bind(task : Task, position : Int) {
        this.task = task
        if(task.urgent) { this.color.setBackgroundColor(Color.RED) } else { this.color.setBackgroundColor(Color.GREEN) }
        this.etxtTask.text = task.nome
        this.doneTask.isChecked = task.done

        //Vai aparecer se está selecionado ou não dentro
        itemView.isSelected = this.adapter.selectedTasks.get(this.task.id) != null

        this.doneTask.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }

    //Disparado quando clicar na Task, ou agora o Switch
    //O onClick executa onClick nosso
    override fun onClick(v: View?) {
        this.task.done = this.doneTask.isChecked
        this.adapter.getOnClickTaskListener()?.onClick(this.task) //se o onclick for diferente de null
    }

    override fun onLongClick(v: View?): Boolean { //true consome o efeito de click e false para nao consumir o efeitod e click
        itemView.isSelected = !itemView.isSelected
        if(this.adapter.selectedTasks.get(this.task.id) != null) {
            //Remover o id que não está selecionado mais
            this.adapter.selectedTasks.delete(this.task.id)
        } else {
            //Adicionando o id que está selecionado
            this.adapter.selectedTasks.append(this.task.id, this.task)
        }
        val newSelectedArray = this.adapter.selectedTasks.clone()
        this.adapter.getOnChangeSelectionListener()?.onChangeSelection(newSelectedArray)
        return true //se o onclick for diferente de null
    }
}