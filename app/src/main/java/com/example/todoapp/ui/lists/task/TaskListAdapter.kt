package com.example.todoapp.ui.lists.task

import android.util.SparseArray
import android.util.SparseBooleanArray
import android.util.SparseIntArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.model.Task

class TaskListAdapter(

    private var taskList : ArrayList<Task>

) : RecyclerView.Adapter<TaskListViewHolder>(){

    private var taskListInitial = this.taskList
    private var listener : OnClickTaskListener? = null
    private var selectionListener : OnChangeSelectionListener? = null

    //Um array que contém os id's dos itens selecionados
    val selectedTasks : SparseArray<Task> = SparseArray<Task>()

    //Criando uma interface para receber a implementacao do evento de click
    fun interface OnClickTaskListener {
        fun onClick(task : Task)
    }

    fun interface OnChangeSelectionListener{
        fun onChangeSelection(selectedIds: SparseArray<Task>)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.card_task, parent, false)
        return TaskListViewHolder(itemView, this)
    }

    //Recicla os valores que estao na itemView, usando o ViewHolder
    override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {
        holder.bind(this.taskList[position], position)
    }

    override fun getItemCount(): Int {
        return this.taskList.size
    }

    //==============================================================================================
    //Adiciona eventos os listeners
    fun setOnClickTaskListener(listener : OnClickTaskListener) : TaskListAdapter {
        this.listener = listener
        return this
    }

    fun getOnClickTaskListener() : OnClickTaskListener? {
        return this.listener
    }
    //==============================================================================================


    //==============================================================================================
    //Adiciona eventos os listeners
    fun setOnChangeSelectionListener(selectionListener : OnChangeSelectionListener) : TaskListAdapter {
        this.selectionListener = selectionListener
        return this
    }

    fun getOnChangeSelectionListener() : OnChangeSelectionListener? {
        return this.selectionListener
    }
    //==============================================================================================


    fun filterTaskDone(filter : Boolean){
        if(filter){
            this.taskList = this.taskList.filter { !it.done } as ArrayList<Task>
        } else {
            this.taskList = this.taskListInitial
        }
        notifyDataSetChanged() //Faça a atualização da lista -> como tivesse apagando e colocando tudo outra vez
    }


}